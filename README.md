KOSapi Abstraction Layer for Java
=================================

About
-----

This project is intended to be a library for [KOSapi-3](https://kosapi.fit.cvut.cz/) clients. Add it as a [Maven](https://maven.apache.org/) dependency.

License
-------

Copyright Ondřej Guth, 2019, [Faculty of Information Technology](https://fit.cvut.cz) of [Czech Technical University in Prague](https://www.cvut.cz/).