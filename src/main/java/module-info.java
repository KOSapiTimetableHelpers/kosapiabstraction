module cz.cvut.fit.kosapi2timetabler.kosapi_data_provider {
    requires java.json;
    requires atom.jaxb;
    requires java.xml.bind;
    exports cz.cvut.fit.kosapi2timetabler.dto;
    exports cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;
    exports cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;
}
