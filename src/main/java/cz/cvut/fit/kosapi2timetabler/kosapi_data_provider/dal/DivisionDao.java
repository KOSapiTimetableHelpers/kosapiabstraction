/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Division;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.DivisionJaxb;
import java.util.Collection;

/**
 *
 * @author guthondr
 */
public class DivisionDao extends Dao<DivisionJaxb, Division> {

    public DivisionDao() {
        super("divisions/18000/subdivisions",
                (d, i) -> new Division(i, d.getCode(), d.getName()));
    }

    public Collection<Division> getAll() {
        return getAllFeedEntries(DivisionJaxb.class);
    }
}
