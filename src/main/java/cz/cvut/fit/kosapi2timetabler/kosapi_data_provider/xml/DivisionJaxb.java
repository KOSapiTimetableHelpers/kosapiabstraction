/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import cz.cvut.fit.kosapi2timetabler.dto.Semester;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class DivisionJaxb implements Serializable {

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String code;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
}
