/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonJaxb {

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String username;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String firstName;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String lastName;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String personalNumber;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String email;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
