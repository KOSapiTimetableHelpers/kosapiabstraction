package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Global KOSapi settings
 *
 * @author guthondr
 */
public class KosapiSettings {

    private static KosapiSettings singletonInstance = null;
    private final int limitPager = 1000;
    private Properties settings = new Properties();
    private Path store;
    public static final String ACCESS_TOKEN_PROP_NAME = "oauthAccessToken";
    public static final String CLIENT_ID_PROP_NAME = "oauthClientID";
    public static final String CLIENT_SECRET_PROP_NAME = "oauthClientSecret";

    public static KosapiSettings getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new KosapiSettings();
        }
        return singletonInstance;
    }

    public URI getBaseUri() {
        try {
            return new URI("https://kosapi.fit.cvut.cz/api/3/");
        } catch (final URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getAccessToken() {
        return settings.getProperty(ACCESS_TOKEN_PROP_NAME);
    }

    public String getClientId() {
        return settings.getProperty(CLIENT_ID_PROP_NAME);
    }

    public String getClientSecret() {
        return settings.getProperty(CLIENT_SECRET_PROP_NAME);
    }
    
    private void updateFileStore() throws SettingsConfigurationFileException {
        if (store != null) {
            try {
                settings.store(Files.newBufferedWriter(store), null);
            } catch (final IOException ex) {
                throw new SettingsConfigurationFileException(ex);
            }
        }
    }

    public void setAccessToken(final String value) throws SettingsConfigurationFileException {
        settings.setProperty(ACCESS_TOKEN_PROP_NAME, value);
        updateFileStore();
    }
    
    public void setClientId(final String value) throws SettingsConfigurationFileException {
        settings.setProperty(CLIENT_ID_PROP_NAME, value);
        updateFileStore();
    }

    public void setClientSecret(final String value) throws SettingsConfigurationFileException {
        settings.setProperty(CLIENT_SECRET_PROP_NAME, value);
        updateFileStore();
    }

    public void refreshAccessToken() throws SettingsConfigurationFileException {
        setAccessToken(AccessTokenRetriever.retrieve());
    }

    public int getLimit() {
        return limitPager;
    }

    public void setSettings(final Properties s) {
        settings = s;
    }

    public void setFileStoreNoLoadSettings(final Path f) {
//        if (!Files.isReadable(f))
//            throw new SettingsConfigurationFileException(null);
        store = f;
    }

    public void setFileStoreLoadSettings(final Path f) {
        setFileStoreNoLoadSettings(f);
        try {
            settings.load(Files.newBufferedReader(store));
        } catch (final IOException ex) {
            throw new SettingsConfigurationFileException(ex);
        }
    }
}
