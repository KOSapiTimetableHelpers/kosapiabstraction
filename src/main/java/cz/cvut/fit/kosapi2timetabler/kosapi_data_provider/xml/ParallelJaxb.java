/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class ParallelJaxb {
    
    @XmlType
    @XmlEnum
    public enum ParallelType {
        LABORATORY, LECTURE, TUTORIAL, UNDEFINED
    }

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Short capacity;   
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Integer code;   
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Xlink course;   
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private ParallelType parallelType;   
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Collection<Xlink> teacher;   

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Collection<TimetableSlotJaxb> timetableSlot;

    public Short getCapacity() {
        return capacity;
    }

    public void setCapacity(Short capacity) {
        this.capacity = capacity;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Xlink getCourse() {
        return course;
    }

    public void setCourse(Xlink course) {
        this.course = course;
    }

    public ParallelType getParallelType() {
        return parallelType;
    }

    public void setParallelType(ParallelType parallelType) {
        this.parallelType = parallelType;
    }

    public Collection<Xlink> getTeacher() {
        return teacher;
    }

    public void setTeacher(Collection<Xlink> teacher) {
        this.teacher = teacher;
    }

    public Collection<TimetableSlotJaxb> getTimetableSlot() {
        return timetableSlot;
    }

    public void setTimetableSlot(Collection<TimetableSlotJaxb> timetableSlot) {
        this.timetableSlot = timetableSlot;
    }
    
    
}
