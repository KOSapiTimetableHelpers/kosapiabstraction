/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;

import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.SemesterJaxb;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal.SemesterDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Properties;
import javax.xml.bind.JAXBException;

/**
 *
 * @author guthondr
 */
public class Test {

    public static void main(String[] args) throws JAXBException, MalformedURLException, IOException {
        String SETTINGS_FILENAME_DEFAULT = ".timetable_aggregator.properties";
        File SETTINGS_FILE = new File(
                System.getProperty("cz.cvut.fit.kosapi2timetable.timetable_aggregator.settings_file", Paths.get(System.getProperty("user.home"), SETTINGS_FILENAME_DEFAULT).toString()));
        final Properties settings = new Properties();
        settings.load(new FileInputStream(SETTINGS_FILE));
        KosapiSettings.getInstance().setSettings(settings);

        SemesterDao semDao = new SemesterDao();
//        Collection<Semester> result = semDao.getAll();
//        System.out.println(result);
//        final JAXBContext jc = JAXBContext.newInstance(Entry.class, SemesterJaxb.class);
//        final Unmarshaller unmarshaller = jc.createUnmarshaller();
////        final Entry entry = (Entry)unmarshaller.unmarshal(
////                new URL("https://kosapi.fit.cvut.cz/api/3/semesters/scheduling?access_token=93f437a3-6bc6-4631-b256-b7a7d0e0542f"));
//        final Entry entry = (Entry)unmarshaller.unmarshal(new File("/tmp/BI-AAG.xml"));
//        Node contentNode = (Node) entry.getContent();
//        SemesterJaxb course = (SemesterJaxb) unmarshaller.unmarshal(contentNode);
//        System.out.println(course.getCode());
    }
}
