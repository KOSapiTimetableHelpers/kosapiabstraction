/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Person;
import cz.cvut.fit.kosapi2timetabler.dto.Semester;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.PersonJaxb;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.SemesterJaxb;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 *
 * @author guthondr
 */
public class TeacherDao extends Dao<PersonJaxb, Person> {

    public TeacherDao() {
        super("teachers",
                (s, i) -> new Person(
                        i, s.getUsername(), s.getFirstName(), s.getLastName(), s.getPersonalNumber(), s.getEmail()
                ));
    }

    public Person getByUsername(final String username) throws URISyntaxException {
        return getOneEntryContent(username, PersonJaxb.class);
    }
}
