package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class AccessTokenRetriever {

    final static String TOKEN_ENDPOINT = "https://auth.fit.cvut.cz/oauth/oauth/token";

    public static String retrieve()  {
        final KosapiSettings settings = KosapiSettings.getInstance();
        final String accessToken;
        try {
            final URL endpoint = new URL(TOKEN_ENDPOINT);
            final HttpURLConnection conn = (HttpURLConnection) endpoint.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic "
                    + Base64.getEncoder().withoutPadding().encodeToString((settings.getClientId() + ':' + settings.getClientSecret()).getBytes()));
            conn.setDoInput(true);
            conn.setDoOutput(true);
            final StringBuilder postData = new StringBuilder("grant_type=client_credentials");
            try (Writer post = new OutputStreamWriter(conn.getOutputStream())) {
                post.write(postData.toString());
            }
            final JsonReader valRespRdr = Json.createReader(conn.getInputStream());
            final JsonObject valRespObj = valRespRdr.readObject();
            accessToken = valRespObj.getString("access_token");
            conn.disconnect();
            return accessToken;
        } catch (final MalformedURLException | ProtocolException ex) {
            throw new RuntimeException(ex);
        } catch (final IOException ex) {
            throw new CannotRetrieveAccessTokenException(ex);
        }
    }
}
