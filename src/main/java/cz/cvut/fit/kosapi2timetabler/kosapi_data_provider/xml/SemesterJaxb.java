/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import cz.cvut.fit.kosapi2timetabler.dto.Semester;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class SemesterJaxb implements Serializable {

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String name;
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Date startDate = new Date();
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Date endDate;
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String code;
    
    public SemesterJaxb() {}
    
    public SemesterJaxb(final Semester s) {
        name = s.getName();
        code = s.getKosId();
        startDate = s.getStartDate();
        endDate = s.getEndDate();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return code + '(' + name + ')';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SemesterJaxb other = (SemesterJaxb) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
    
    

    
}
