/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;

import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.Xlink;

/**
 *
 * @author guthondr
 */
public class KosapiUtils {
    public static String kosidFromAtomid(final String atomId) {
        final String [] parts = atomId.split(":");
        return parts[parts.length-1];
    }
    
    public static String codeFromXlink(final Xlink xlink) {
        final String [] parts = xlink.getHref().split("/");
        return parts[1];
    }
}
