/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Semester;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.SemesterJaxb;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.KosapiSettings;
import cz.jirutka.atom.jaxb.AtomLink;
import cz.jirutka.atom.jaxb.Entry;
import cz.jirutka.atom.jaxb.Feed;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import org.w3c.dom.Node;

/**
 *
 * @author guthondr
 */
public abstract class Dao<T, V> {

    protected final KosapiSettings settings = KosapiSettings.getInstance();
    private final String relResourceParametrized;
    private final BiFunction<T, String, V> contentToDtoFunction;
    private String semesterCode;

    /**
     * 
     * @param relResource Relative URI of a resource (without query parameters)
     * @param contentToDtoFunction How to create a V from a T
     */
    protected Dao(final String relResource, final BiFunction<T, String, V> contentToDtoFunction) {
        relResourceParametrized = relResource;
        this.contentToDtoFunction = contentToDtoFunction;
    }

    protected void setSemester(final SemesterJaxb s) {
        this.semesterCode = s.getCode();
    }

    protected void setSemester(final Semester semester) {
        this.semesterCode = semester.getKosId();
    }
    
    protected void setSemester(final String semesterCode) {
        this.semesterCode = semesterCode;
    }
    
    private URI addQueryParam(final URI uri, final String param) throws URISyntaxException {
        if (param == null)
            return uri;
        else
            return new URI(uri.getScheme(), uri.getHost(), uri.getPath(), 
                                uri.getQuery() == null 
                                        ? param : uri.getQuery().concat("&" + param), uri.getFragment());
    }

    private Object unmarshallFromUri(final URI uri, final Unmarshaller unmarshaller) {
        try {
            try {
                return unmarshaller.unmarshal(addQueryParam(uri, "access_token=" + settings.getAccessToken()).toURL());
            } catch (final UnmarshalException ex) {
                if (ex.getLinkedException().getMessage().contains("Server returned HTTP response code: 401")) {
                    settings.refreshAccessToken();
                    return unmarshaller.unmarshal(addQueryParam(uri, "access_token=" + settings.getAccessToken()).toURL());
                } else {
                    throw new RuntimeException(ex);
                }
            }
        } catch (final JAXBException | MalformedURLException | URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    private V dtoFromEntry(final Entry entry, final Unmarshaller unmarshaller) throws JAXBException {
        final Node contentNode = (Node) entry.getContent();
        final T content = (T) unmarshaller.unmarshal(contentNode);
        return contentToDtoFunction.apply(content, entry.getId());
    }

    private List<V> getAllEntriesContentFromPage(final URI pageUriRel,
            final Unmarshaller unmarshaller) throws URISyntaxException  {
        final List<V> result = new ArrayList<>();
        final Feed<Entry> root = (Feed) unmarshallFromUri(
                settings.getBaseUri().resolve(pageUriRel), unmarshaller);
        for (final Entry entry : root.getEntries()) {
            try {
                result.add(dtoFromEntry(entry, unmarshaller));
            } catch (JAXBException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    private URI constructRelQueryWoAccessToken(final String resource, final String queryRsql, final String... parameters) 
            throws URISyntaxException {
        final String resourceURI = parameters == null ? resource : String.format(resource, (Object[])parameters);
        return new URI(null, null, resourceURI, "limit=" + settings.getLimit() + "&includeInvalidSlots=true"
                + (semesterCode == null ? "" : "&sem=" + semesterCode)
                + (queryRsql == null ? "" : "&query=" + queryRsql), null);
    }
    
    protected List<V> getAllFeedEntries(final String queryRsql, final Class<T> clazz, final String... parameters) {
        try {
            final JAXBContext jc = JAXBContext.newInstance(Feed.class, Entry.class, clazz);
            final Unmarshaller um = jc.createUnmarshaller();
            Feed<Entry> root = (Feed) unmarshallFromUri(settings.getBaseUri().resolve(
                    constructRelQueryWoAccessToken(relResourceParametrized, queryRsql, parameters)),
                    um);
            final List<V> result = getAllEntriesContentFromPage(
                    constructRelQueryWoAccessToken(relResourceParametrized, queryRsql, parameters), 
                    um);
            boolean newRootFound;
            do {
                newRootFound = false;
                for (final AtomLink l : root.getLinks()) {
                    if (l.getRel().equals(AtomLink.NEXT)) {
                        root = (Feed<Entry>) unmarshallFromUri(
                                settings.getBaseUri().resolve(l.getHref()), um);
                        result.addAll(getAllEntriesContentFromPage(
                                l.getHref(), um));
                        newRootFound = true;
                        break;
                    }
                }
            } while (newRootFound);
            return result;
        } catch (final JAXBException | URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected List<V> getAllFeedEntries(final Class<T> clazz, final String... parameters) {
        return getAllFeedEntries(null, clazz, parameters);
    }

    protected V getOneEntryContent(final String subPath, final String queryRsql,
            final Class<T> clazz, final String... parameters) throws URISyntaxException {
        try {
            final JAXBContext jc = JAXBContext.newInstance(Entry.class, clazz);
            final Unmarshaller unmarshaller = jc.createUnmarshaller();
            final Entry entry = (Entry) unmarshallFromUri(settings.getBaseUri().resolve(
                    constructRelQueryWoAccessToken(relResourceParametrized + '/' + subPath, queryRsql, parameters)),
                    unmarshaller);
            return dtoFromEntry(entry, unmarshaller);
        } catch (final JAXBException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected V getOneEntryContent(final String subPath, final Class<T> clazz, final String... parameters) throws URISyntaxException {
        return getOneEntryContent(subPath, null, clazz, parameters);
    }

}
