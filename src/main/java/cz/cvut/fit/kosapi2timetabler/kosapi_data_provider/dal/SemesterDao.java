/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Semester;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.SemesterJaxb;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 *
 * @author guthondr
 */
public class SemesterDao extends Dao<SemesterJaxb, Semester> {

    public SemesterDao() {
        super("semesters",
                (s, i) -> new Semester(
                        i, s.getName(), s.getStartDate(), s.getEndDate()
                ));
    }

    public Collection<Semester> getAll() {
        return getAllFeedEntries(SemesterJaxb.class).stream()
                .sorted(Comparator.comparing((Semester s) -> s.getStartDate()).reversed())
                .collect(Collectors.toList());
    }

    public Semester getSemesterByCodeOrAlias(final String codeOrAlias) throws URISyntaxException {
        return getOneEntryContent(codeOrAlias, SemesterJaxb.class);
    }

    public Semester getCurrent() {
        try {
            return getSemesterByCodeOrAlias("current");
        } catch (final URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Semester getNext() {
        try {
            return getSemesterByCodeOrAlias("next");
        } catch (final URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Semester getScheduling() {
        try {
            return getSemesterByCodeOrAlias("scheduling");
        } catch (final URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }
}
