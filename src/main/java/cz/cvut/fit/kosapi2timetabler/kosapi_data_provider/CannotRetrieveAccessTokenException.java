package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;

/**
 * 
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
public class CannotRetrieveAccessTokenException extends RuntimeException {
    public CannotRetrieveAccessTokenException(final Exception cause) {
        super(cause);
    }
}
