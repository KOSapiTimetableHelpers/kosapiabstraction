/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Person;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.PersonJaxb;
import java.util.Collection;

/**
 *
 * @author guthondr
 */
public class StudentDao extends Dao<PersonJaxb, Person> {
    public StudentDao(final String semesterCode) {
        super("courses/%s/students", 
                (PersonJaxb s, String i) -> new Person(
                       i, s.getUsername(), s.getFirstName(), s.getLastName(), s.getPersonalNumber(), s.getEmail()
                ));
        setSemester(semesterCode);
    }
    
    public Collection<Person> getAllEnrolledToCourse(final String courseCode) {
        return getAllFeedEntries(
                PersonJaxb.class,
                courseCode);
    }
}
