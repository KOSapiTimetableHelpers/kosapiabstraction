/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Parallel;
import cz.cvut.fit.kosapi2timetabler.dto.TimetableSlot;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.KosapiUtils;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.ParallelJaxb;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import javax.xml.bind.UnmarshalException;

/**
 *
 * @author guthondr
 */
public abstract class ParallelDao extends Dao<ParallelJaxb, Parallel> {
    protected ParallelDao(final String semesterCode, final String resourceUri) {
        super(resourceUri + "/%s/parallels", 
                (ParallelJaxb p, String i) -> new Parallel(
                       i, 
                        p.getCapacity(), 
                        p.getCode(), 
                        KosapiUtils.codeFromXlink(p.getCourse()), 
                        p.getParallelType().toString(), 
                        p.getTimetableSlot() == null ? null : p.getTimetableSlot()
                                .stream().map(s -> new TimetableSlot(
                                        s.getId().toString(), s.getDay(), 
                                        s.getDuration(), s.getFirstHour(), 
                                        s.getParity().toString(), 
                                        s.getRoom() == null ? null : KosapiUtils.codeFromXlink(s.getRoom())))
                                .collect(Collectors.toList()),
                        p.getTeacher() == null ? null : p.getTeacher().stream().map(x -> KosapiUtils.codeFromXlink(x)).collect(Collectors.toList())
                ));
        setSemester(semesterCode);
    }
    
    public Collection<Parallel> getByCode(final String codeOrUsername) {
        try {
            return getAllFeedEntries(ParallelJaxb.class, codeOrUsername);
        } catch (final RuntimeException ex) {
            if (ex.getCause() instanceof IOException || ex.getCause() instanceof UnmarshalException) {
                System.err.println(ex);
                return Collections.EMPTY_LIST;
            } else
                throw ex;
        }
    }
}
