/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://kosapi.feld.cvut.cz/schema/3", name = "timetableSlot")
@XmlAccessorType(XmlAccessType.FIELD)
public class TimetableSlotJaxb implements Serializable {

    @XmlType
    @XmlEnum(String.class)
    public enum Parity {
        ODD, EVEN, BOTH, UNDEFINED
    }

    @XmlAttribute
    private Long id;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Byte day;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Byte duration;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Byte firstHour;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Parity parity;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Xlink room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte getDay() {
        return day;
    }

    public void setDay(Byte day) {
        this.day = day;
    }

    public Byte getDuration() {
        return duration;
    }

    public void setDuration(Byte duration) {
        this.duration = duration;
    }

    public Byte getFirstHour() {
        return firstHour;
    }

    public void setFirstHour(Byte firstHour) {
        this.firstHour = firstHour;
    }

    public Parity getParity() {
        return parity;
    }

    public void setParity(Parity parity) {
        this.parity = parity;
    }

    public Xlink getRoom() {
        return room;
    }

    public void setRoom(Xlink room) {
        this.room = room;
    }
    
    

}
