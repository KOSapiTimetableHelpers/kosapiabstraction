/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Course;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.KosapiUtils;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.CourseJaxb;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author guthondr
 */
public class CourseDao extends Dao<CourseJaxb, Course> {
    public CourseDao(final String semesterCode) {
        super("courses", 
                (CourseJaxb s, String i) -> new Course(
                       i, s.getCode(), s.getName(), 
                        KosapiUtils.codeFromXlink(s.getDepartment()), 
                        (short)0, (short)0, (short)0, 
                        s.getInstance().getInstructors() == null ? null :
                            s.getInstance().getInstructors().stream().map(KosapiUtils::codeFromXlink).collect(Collectors.toList()), 
                        s.getInstance().getLecturers() == null ? null :
                            s.getInstance().getLecturers().stream().map(KosapiUtils::codeFromXlink).collect(Collectors.toList())
                ));
        setSemester(semesterCode);
    }

    public Collection<Course> getAll() {
        return getAllFeedEntries(
                "(department==18000 or department==18100 or department==18101 or department==18102 or department==18103 or department==18104 or department==18105 or department==18106)",
                CourseJaxb.class).stream()
                .sorted(Comparator.comparing((Course c) -> c.getCode()))
                .collect(Collectors.toList());
    }
}
