/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://kosapi.feld.cvut.cz/schema/3", name = "capacity")
@XmlAccessorType(XmlAccessType.FIELD)
public class CapacityJaxb implements Serializable {

    @XmlAttribute
    private String of;
    
    @XmlAttribute(name = "for")
    private String capacityFor;
    
    @XmlValue
    private Short value;

    public String getOf() {
        return of;
    }

    public void setOf(String of) {
        this.of = of;
    }

    public String getFor() {
        return capacityFor;
    }

    public void setFor(String capacityFor) {
        this.capacityFor = capacityFor;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }
    
    
    
}
