/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

import cz.cvut.fit.kosapi2timetabler.dto.Room;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.CapacityJaxb;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml.RoomJaxb;
import java.util.Collection;
import java.util.Optional;

/**
 *
 * @author guthondr
 */
public class RoomDao extends Dao<RoomJaxb, Room> {

    public RoomDao() {
        super("rooms",
                (RoomJaxb r, String i) -> {
                    final Optional<CapacityJaxb> o = 
                            (r.getCapacity() == null) ? Optional.empty()
                            : r.getCapacity().stream().filter(c -> c.getFor().equals("teaching")).findFirst();
                    return new Room(
                        i, 
                        r.getCode(), 
                        o.isPresent() ? o.get().getValue() : 0);
                });
    }

    public Collection<Room> getAll() {
        return getAllFeedEntries(RoomJaxb.class);
    }
}
