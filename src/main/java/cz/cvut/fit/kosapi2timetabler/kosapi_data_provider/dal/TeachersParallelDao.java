/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.dal;

/**
 *
 * @author guthondr
 */
public class TeachersParallelDao extends ParallelDao {
    public TeachersParallelDao(final String semesterCode) {
        super(semesterCode, "teachers");
    }
}
