/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class RoomJaxb {

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String code;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<CapacityJaxb> capacity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }    

    public List<CapacityJaxb> getCapacity() {
        return capacity;
    }

    public void setCapacity(List<CapacityJaxb> capacity) {
        this.capacity = capacity;
    }
    
    
}
