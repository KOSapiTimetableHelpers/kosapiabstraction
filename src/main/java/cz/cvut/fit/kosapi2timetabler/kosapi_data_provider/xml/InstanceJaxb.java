/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://kosapi.feld.cvut.cz/schema/3", name = "instance")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstanceJaxb implements Serializable {

    @XmlAttribute
    private String semester;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<CapacityJaxb> capacity;
    
    @XmlElementWrapper(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    @XmlElement(name = "teacher", namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<Xlink> examiners;

    @XmlElementWrapper(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    @XmlElement(name = "teacher", namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<Xlink> guarantors;

    @XmlElementWrapper(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    @XmlElement(name = "teacher", namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<Xlink> instructors;

    @XmlElementWrapper(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    @XmlElement(name = "teacher", namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private List<Xlink> lecturers;
    
    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Short occupied;

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Collection<CapacityJaxb> getCapacity() {
        return capacity;
    }

    public void setCapacity(List<CapacityJaxb> capacity) {
        this.capacity = capacity;
    }

    public Collection<Xlink> getExaminers() {
        return examiners;
    }

    public void setExaminers(List<Xlink> examiners) {
        this.examiners = examiners;
    }

    public Collection<Xlink> getGuarantors() {
        return guarantors;
    }

    public void setGuarantors(List<Xlink> guarantors) {
        this.guarantors = guarantors;
    }

    public Collection<Xlink> getInstructors() {
        return instructors;
    }

    public void setInstructors(List<Xlink> instructors) {
        this.instructors = instructors;
    }

    public Collection<Xlink> getLecturers() {
        return lecturers;
    }

    public void setLecturers(List<Xlink> lecturers) {
        this.lecturers = lecturers;
    }

    public Short getOccupied() {
        return occupied;
    }

    public void setOccupied(final Short occupied) {
        this.occupied = occupied;
    }
}
