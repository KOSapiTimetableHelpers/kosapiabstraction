/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guthondr
 */
@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
public class CourseJaxb {

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String code;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private String name;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private Xlink department;

    @XmlElement(namespace = "http://kosapi.feld.cvut.cz/schema/3")
    private InstanceJaxb instance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Xlink getDepartment() {
        return department;
    }

    public void setDepartment(Xlink department) {
        this.department = department;
    }

    public InstanceJaxb getInstance() {
        return instance;
    }

    public void setInstance(InstanceJaxb instance) {
        this.instance = instance;
    }

    @Override
    public String toString() {
        return code;
    }
    
    

}
