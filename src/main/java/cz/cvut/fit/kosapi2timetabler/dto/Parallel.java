/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author guthondr
 */
public class Parallel extends KosapiDto {
    private final Short capacity;
    private final Integer code;
    private final String courseCode;
    private final String paralleltype;
    private final Collection<TimetableSlot> ttSlots;
    private final List<String> teacherUsernames;

    public Parallel(String atomId, Short capacity, Integer code, String courseCode, 
            String paralleltype, Collection<TimetableSlot> ttSlots,
            List<String> teacherUsernames) {
        super(atomId);
        this.capacity = capacity;
        this.code = code;
        this.courseCode = courseCode;
        this.paralleltype = paralleltype;
        this.ttSlots = (ttSlots == null ? Collections.EMPTY_LIST : Collections.unmodifiableCollection(ttSlots));
        this.teacherUsernames = (teacherUsernames == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(teacherUsernames));
    }

    public Short getCapacity() {
        return capacity;
    }

    public Integer getCode() {
        return code;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public String getParalleltype() {
        return paralleltype;
    }

    public Collection<TimetableSlot> getTtSlots() {
        return ttSlots;
    }

    public List<String> getTeacherUsernames() {
        return teacherUsernames;
    }
    
    
}
