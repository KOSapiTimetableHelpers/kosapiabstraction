/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

/**
 *
 * @author guthondr
 */
public class TimetableSlot extends KosapiDto {
    private final Byte day;
    private final Byte duration;
    private final Byte firstHour;
    private final String parity;
    private final String roomCode;

    public TimetableSlot(String atomId, Byte day, Byte duration, Byte firstHour, String parity, String roomCode) {
        super(atomId);
        this.day = day;
        this.duration = duration;
        this.firstHour = firstHour;
        this.parity = parity;
        this.roomCode = roomCode;
    }

    public Byte getDay() {
        return day;
    }

    public Byte getDuration() {
        return duration;
    }

    public Byte getFirstHour() {
        return firstHour;
    }

    public String getParity() {
        return parity;
    }

    public String getRoomCode() {
        return roomCode;
    }
}
