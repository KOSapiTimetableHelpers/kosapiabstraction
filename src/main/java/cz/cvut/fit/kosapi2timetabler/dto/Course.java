/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

import java.util.Collection;

/**
 *
 * @author guthondr
 */
public class Course extends KosapiDto {

    private final String code;
    private final String name;
    private final String departmentCode;
    private final short totalCapacity;
    private final short tutorialCapacity;
    private final short occupied;
    private final Collection<String> instructorsLogins;
    private final Collection<String> lecturersLogins;

    public Course(String atomId, String code, String name, String deptCode, 
            short totCapacity, short tutCapacity, short enrolledStuds, 
            Collection<String> instructors, Collection<String> lecturers) {
        super(atomId);
        this.code = code;
        this.name = name;
        this.departmentCode = deptCode;
        this.totalCapacity = totCapacity;
        this.tutorialCapacity = tutCapacity;
        this.occupied = enrolledStuds;
        this.instructorsLogins = instructors;
        this.lecturersLogins = lecturers;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public short getTotalCapacity() {
        return totalCapacity;
    }

    public short getTutorialCapacity() {
        return tutorialCapacity;
    }

    public short getOccupied() {
        return occupied;
    }

    public Collection<String> getInstructorsLogins() {
        return instructorsLogins;
    }

    public Collection<String> getLecturersLogins() {
        return lecturersLogins;
    }

    @Override
    public String toString() {
        return getCode();
    }
    
    

    
}
