/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.KosapiUtils;
import java.util.Objects;

/**
 *
 * @author guthondr
 */
public abstract class KosapiDto {

    protected final String kosId;

    public KosapiDto(final String atomId) {
        this.kosId = KosapiUtils.kosidFromAtomid(atomId);
    }

    public String getKosId() {
        return kosId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.kosId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KosapiDto other = (KosapiDto) obj;
        if (!Objects.equals(this.kosId, other.kosId)) {
            return false;
        }
        return true;
    }
    
}
