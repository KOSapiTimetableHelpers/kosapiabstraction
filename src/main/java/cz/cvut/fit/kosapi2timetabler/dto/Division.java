/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

/**
 *
 * @author guthondr
 */
public class Division extends KosapiDto {
    private final String code;
    private final String title;


    public Division(final String atomId, final String code, final String title) {
        super(atomId);
        this.code = code;
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }
}
