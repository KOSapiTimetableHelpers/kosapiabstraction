/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

/**
 *
 * @author guthondr
 */
public class Person extends KosapiDto {
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String personalNumber;
    private final String email;

    public Person(String atomId, String username, String firstName, String lastName, String personalNumber, String email) {
        super(atomId);
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalNumber = personalNumber;
        this.email = email;
    }
    
    

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public String getEmail() {
        return email;
    }
    
    
}
