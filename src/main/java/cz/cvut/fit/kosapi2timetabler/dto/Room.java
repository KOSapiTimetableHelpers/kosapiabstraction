/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

/**
 *
 * @author guthondr
 */
public class Room extends KosapiDto {
    private final String code;
    private final short capacityTeaching;
    
    public Room(final String atomId, final String code, final short capTeach) {
        super(atomId);
        this.code = code;
        this.capacityTeaching = capTeach;
    }

    public String getCode() {
        return code;
    }

    public short getCapacityTeaching() {
        return capacityTeaching;
    }
    
    
    
}
