/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.dto;

import java.util.Date;

/**
 *
 * @author guthondr
 */
public class Semester extends KosapiDto {
    private final String name;
    private final Date startDate;
    private final Date endDate;

    public Semester(String atomId, String name, Date startDate, Date endDate) {
        super(atomId);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public String toString() {
        return getKosId();
    }
    
    
}
